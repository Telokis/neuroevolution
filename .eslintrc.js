module.exports = {
    extends: ["@telokys/eslint-config-react-typescript"],
    rules: {
        "no-console": "off",
        "no-new": "off",
        "new-cap": "off",
        "jest/prefer-strict-equal": "off",
        "@typescript-eslint/no-empty-function": "off",
        "react/jsx-props-no-spreading": "off",
    },
};
