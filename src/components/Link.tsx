import React, { FC } from "react";
import { Icon } from "@material-ui/core";

interface LinkProps extends React.AnchorHTMLAttributes<HTMLAnchorElement> {
    to: string;
}

const Link: FC<LinkProps> = (props: LinkProps) => {
    const { children, to, ...otherProps } = props;

    return (
        <a {...otherProps} href={to} rel="noopener noreferrer" target="_blank">
            {children}
            <Icon style={{ marginLeft: "0.2rem", marginBottom: "-0.2rem", fontSize: "17px" }}>
                open_in_new
            </Icon>
        </a>
    );
};

Link.defaultProps = {};

export default Link;
