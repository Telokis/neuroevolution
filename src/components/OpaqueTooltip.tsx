import { Tooltip, withStyles } from "@material-ui/core";

const OpaqueTooltip = withStyles(() => ({
    tooltip: {
        backgroundColor: "rgba(97, 97, 97, 0.975)",
    },
}))(Tooltip);

export default OpaqueTooltip;
