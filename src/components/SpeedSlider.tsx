import React, { FC, useMemo, useState, useEffect } from "react";
import { Slider, Typography, Tooltip } from "@material-ui/core";

export interface Speed {
    ticks: number;
    fps: number;
}

interface SpeedSliderProps {
    maxFPS?: number;
    max?: number;
    defaultValue?: Speed;
    value?: Speed;
    onUpdate?: (speed: Speed, rawValue: number) => void;
}

const computeValuesFactory = (maxFPS: number) => (value: number): Speed => {
    const res = {
        ticks: Math.max(1, value),
        fps: maxFPS,
    };

    if (value < 0) {
        res.fps = maxFPS + 1 - value * -1;
    }

    return res;
};

const makeLabelFactory = (maxFPS: number, compute: (value: number) => Speed) => (value: number) => {
    const values = compute(value);

    if (values.fps === maxFPS) {
        return `${values.ticks} UPF`;
    }

    return `1 UPF @ ${values.fps} FPS`;
};

const valuesToValueFactory = (maxFPS: number) => (speed: Speed) => {
    if (speed.fps < maxFPS) {
        return speed.fps - maxFPS - 1;
    }

    return speed.ticks;
};

const SpeedSlider: FC<SpeedSliderProps> = ({
    maxFPS = 60,
    max = 100,
    value: valueProp,
    defaultValue,
    onUpdate = () => {},
}: SpeedSliderProps) => {
    const computeValues = useMemo(() => computeValuesFactory(maxFPS), [maxFPS]);
    const valuesToValue = useMemo(() => valuesToValueFactory(maxFPS), [maxFPS]);
    const makeLabel = useMemo(() => makeLabelFactory(maxFPS, computeValues), [
        maxFPS,
        computeValues,
    ]);
    const [stateValue, setValue] = useState(defaultValue ?? valueProp ?? computeValues(1));
    const value = useMemo(() => valuesToValue(stateValue), [stateValue, valuesToValue]);

    useEffect(() => {
        if (valueProp) {
            setValue(valueProp);
        }
    }, [valueProp]);

    return (
        <div style={{ marginRight: "10px", marginLeft: "10px" }}>
            <Tooltip
                placement="top"
                arrow
                title={
                    <Typography display="block">
                        FPS = Frames Per Second
                        <br />
                        UPF = Updates Per Frame
                    </Typography>
                }
            >
                <Typography variant="subtitle1" display="inline">
                    Simulation speed{" "}
                    <Typography display="inline" color="textSecondary">
                        ({makeLabel(value)})
                    </Typography>
                </Typography>
            </Tooltip>
            <Slider
                defaultValue={defaultValue ? valuesToValue(defaultValue) : 1}
                value={value}
                valueLabelDisplay="off"
                step={1}
                marks={[
                    { value: -maxFPS, label: "1 FPS" },
                    { value: 1, label: "Default" },
                    { value: max, label: `${max} UPF` },
                ]}
                track={false}
                min={-1 * maxFPS}
                max={max}
                onChange={(_, newVal: number) => {
                    const computedValues = computeValues(newVal);

                    setValue(computedValues);
                    onUpdate(computedValues, newVal);
                }}
            />
        </div>
    );
};

export default SpeedSlider;
