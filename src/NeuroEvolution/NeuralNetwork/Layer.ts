import Random from "./Random";
import Matrix, { IJsonMatrix } from "./Matrix";

export interface IJsonLayer {
    weights: IJsonMatrix;
    biases: IJsonMatrix;
    fromDimension: number;
    toDimension: number;
}

export default class Layer {
    weights: Matrix;

    biases: Matrix;

    fromDimension: number;

    toDimension: number;

    static fromJSON(json: IJsonLayer) {
        const layer = new Layer(json.fromDimension, json.toDimension);

        layer.weights = Matrix.fromJSON(json.weights);
        layer.biases = Matrix.fromJSON(json.biases);

        return layer;
    }

    constructor(from: number, to: number) {
        this.fromDimension = from;
        this.toDimension = to;

        this.weights = new Matrix(to, from);
        this.weights.fill(() => Random.global.float() * 2 - 1);

        this.biases = new Matrix(to, 1);
        this.biases.fill(() => Random.global.float() * 2 - 1);
    }

    feed(values: Array<number>, activationFn: (val: number) => number) {
        const valuesMatrix = Matrix.fromArray(values);
        const result = Matrix.multiply(this.weights, valuesMatrix);

        result.add(this.biases);
        result.fill((_1, _2, val) => activationFn(val));

        return result.toArray();
    }

    toJSON(): IJsonLayer {
        return {
            weights: this.weights.toJSON(),
            biases: this.biases.toJSON(),
            fromDimension: this.fromDimension,
            toDimension: this.toDimension,
        };
    }
}
