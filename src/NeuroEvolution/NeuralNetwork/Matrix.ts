import _ from "lodash";

export interface IJsonMatrix {
    data: Array<Array<number>>;
    rows: number;
    columns: number;
}

export default class Matrix {
    data: Array<Array<number>> = [];

    rows: number;

    columns: number;

    static fromArray(values: Array<number>) {
        return new Matrix(values.length, 1).fill((row) => values[row]);
    }

    static fromJSON(json: IJsonMatrix) {
        const m = new Matrix(json.rows, json.columns);

        m.data = _.cloneDeep(json.data);

        return m;
    }

    constructor(rows: number, columns: number) {
        this.rows = rows;
        this.columns = columns;

        for (let row = 0; row < rows; row++) {
            const tmp: Array<number> = [];

            for (let col = 0; col < columns; col++) {
                tmp.push(row === col ? 1 : 0);
            }

            this.data.push(tmp);
        }
    }

    fill(fn: number | ((row: number, col: number, val: number) => number)) {
        for (let row = 0; row < this.data.length; row++) {
            for (let col = 0; col < this.data[row].length; col++) {
                this.data[row][col] =
                    typeof fn === "number" ? fn : fn(row, col, this.data[row][col]);
            }
        }

        return this;
    }

    static multiply(m1: Matrix, m2: Matrix) {
        if (m1.columns !== m2.rows) {
            throw new Error("m1.columns must equal m2.rows");
        }

        const result = new Matrix(m1.rows, m2.columns);

        result.fill((row, col) => {
            let val = 0;

            for (let i = 0; i < m1.columns; i++) {
                val += m1.data[row][i] * m2.data[i][col];
            }

            return val;
        });

        return result;
    }

    at(row: number, col: number) {
        if (row < 0 || row >= this.rows) {
            throw new Error(`row is out of bounds. ${row} is < 0 or >= ${this.rows}`);
        }

        if (col < 0 || col >= this.columns) {
            throw new Error(`col is out of bounds. ${col} is < 0 or >= ${this.columns}`);
        }

        return this.data[row][col];
    }

    add(val: number | Matrix) {
        if (typeof val === "number") {
            this.fill((_1, _2, v) => v + val);
        } else {
            if (this.rows !== val.rows || this.columns !== val.columns) {
                throw new Error("m1.columns must equal m2.rows");
            }

            this.fill((i, j, v) => v + val.at(i, j));
        }
    }

    toArray() {
        const arr: Array<number> = [];

        for (let i = 0; i < this.data.length; i++) {
            const col = this.data[i];

            for (let j = 0; j < col.length; j++) {
                const row = col[j];

                arr.push(row);
            }
        }

        return arr;
    }

    toJSON(): IJsonMatrix {
        return {
            data: _.cloneDeep(this.data),
            rows: this.rows,
            columns: this.columns,
        };
    }
}
