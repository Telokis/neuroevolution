import p5 from "p5";
import NeuralNetwork, { tanh } from "./NeuralNetwork";
import Layer from "./Layer";

const SPACE_BETWEEN_LAYERS = 200;
const SPACE_BETWEEN_NODES = 10;
const PADDING = 10;
const NODE_RADIUS = 30;

function highestDimension(nn: NeuralNetwork) {
    return nn.layers.reduce((max, layer) => {
        return Math.max(max, layer.toDimension);
    }, nn.inputDimension);
}

function nodeY(index: number, dimension: number, highestDim: number) {
    const origin =
        (highestDim - dimension) * NODE_RADIUS +
        (highestDim - dimension) * 0.5 * SPACE_BETWEEN_NODES;

    return origin + index * NODE_RADIUS * 2 + index * SPACE_BETWEEN_NODES + PADDING + NODE_RADIUS;
}

function getColor(n: number, useAlpha = false): [number, number, number, number] {
    const val = tanh(n / 5) * 255;
    const alpha = Math.max(50, useAlpha ? Math.abs(val) : 255);

    if (val > 0) {
        return [0, val, 0, alpha];
    }

    return [-val, 0, 0, alpha];
}

function nodeX(layerIndex: number) {
    return PADDING + NODE_RADIUS + layerIndex * SPACE_BETWEEN_LAYERS;
}

function drawLayerNodes(
    p: p5,
    layerIndex: number,
    layer: Layer,
    highestDim: number,
    values?: Array<number>,
) {
    const dimension = layer.toDimension;
    p.stroke("black");
    p.strokeWeight(1);

    for (let i = 0; i < dimension; ++i) {
        if (values) {
            p.fill(...getColor(values[i]));
        } else {
            p.fill(...getColor(layer.biases.at(i, 0)));
        }

        p.circle(nodeX(layerIndex), nodeY(i, dimension, highestDim), NODE_RADIUS * 2);
    }
}

function drawInitialNodes(p: p5, dimension: number, highestDim: number, values?: Array<number>) {
    p.noFill();
    p.stroke("black");
    p.strokeWeight(1);

    for (let i = 0; i < dimension; ++i) {
        if (values) {
            p.fill(...getColor(values[i]));
        }

        p.circle(nodeX(0), nodeY(i, dimension, highestDim), NODE_RADIUS * 2);
    }
}

function drawConnections(
    p: p5,
    index: number,
    layer: Layer,
    highestDim: number,
    values?: Array<number>,
) {
    for (let i = 0; i < layer.fromDimension; ++i) {
        const inX = nodeX(index - 1) + NODE_RADIUS;
        const inY = nodeY(i, layer.fromDimension, highestDim);

        for (let j = 0; j < layer.toDimension; ++j) {
            const w = layer.weights.at(j, i) * (values?.[i] ?? 1);

            p.stroke(...getColor(w, false));
            p.strokeWeight(Math.max(Math.floor(tanh((w * w) / 25) * 5), 1));
            p.line(inX, inY, nodeX(index) - NODE_RADIUS, nodeY(j, layer.toDimension, highestDim));
        }
    }
}

export function drawNN(nn: NeuralNetwork, p: p5, inputs?: Array<number>) {
    p.push();

    const highestDim = highestDimension(nn);

    p.stroke("black");
    p.fill(255, 255, 255, 180);
    p.rect(
        0,
        0,
        2 * PADDING + SPACE_BETWEEN_LAYERS * nn.layers.length + NODE_RADIUS * 2,
        2 * PADDING + highestDim * NODE_RADIUS * 2 + SPACE_BETWEEN_NODES * (highestDim - 1),
    );

    let values = inputs;

    drawInitialNodes(p, nn.inputDimension, highestDim, inputs);

    for (let i = 0; i < nn.layers.length; ++i) {
        const layer = nn.layers[i];

        if (inputs) {
            values = layer.feed(values!, nn.activationFunction);
        }

        drawLayerNodes(p, i + 1, layer, highestDim, values);
        drawConnections(p, i + 1, layer, highestDim, values);
    }

    p.pop();
}
