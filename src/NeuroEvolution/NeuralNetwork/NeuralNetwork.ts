import Layer, { IJsonLayer } from "./Layer";

export interface IJsonNeuralNetwork {
    inputDimension: number;
    layers: Array<IJsonLayer>;
}

export function sigmoid(val: number) {
    return 1 / (1 + Math.exp(-val));
}

export function tanh(val: number) {
    return Math.tanh(val);
}

export function relu(val: number) {
    return val > 0 ? val : 0;
}

export function identity(val: number) {
    return val;
}

export default class NeuralNetwork {
    inputDimension: number;

    layers: Array<Layer> = [];

    activationFunction: (val: number) => number;

    static fromJSON(json: IJsonNeuralNetwork) {
        const nn = new NeuralNetwork(json.inputDimension, [], 1);

        nn.layers = json.layers.map((jsonLayer) => Layer.fromJSON(jsonLayer));

        return nn;
    }

    constructor(inputDimension: number, hiddenLayers: Array<number>, ouputNodes: number) {
        this.inputDimension = inputDimension;
        this.activationFunction = relu;

        let lastDim = inputDimension;
        for (let layerIndex = 0; layerIndex < hiddenLayers.length; layerIndex++) {
            const layerDimension = hiddenLayers[layerIndex];

            this.layers.push(new Layer(lastDim, layerDimension));

            lastDim = layerDimension;
        }

        this.layers.push(new Layer(lastDim, ouputNodes));
    }

    predict(inputs: Array<number>) {
        if (inputs.length !== this.inputDimension) {
            throw new Error(
                `Network input has dimension ${this.inputDimension} but inputs was ${inputs.length} long.`,
            );
        }

        let result = inputs;

        for (let i = 0; i < this.layers.length; i++) {
            const layer = this.layers[i];

            result = layer.feed(result, this.activationFunction);
        }

        return result;
    }

    toJSON(): IJsonNeuralNetwork {
        return {
            inputDimension: this.inputDimension,
            layers: this.layers.map((l) => l.toJSON()),
        };
    }
}
