function hashString(str: string) {
    let hash = 0;
    let i = 0;
    let chr = 0;

    if (str.length === 0) {
        return hash;
    }

    for (i = 0; i < str.length; i++) {
        chr = str.charCodeAt(i);
        hash = (hash << 5) - hash + chr;
        hash |= 0; // Convert to 32bit integer
    }

    return hash;
}

function mulberry32(a: number) {
    return () => {
        a += 0x6d2b79f5;
        let t = a;
        t = Math.imul(t ^ (t >>> 15), t | 1);
        t ^= t + Math.imul(t ^ (t >>> 7), t | 61);
        return ((t ^ (t >>> 14)) >>> 0) / 4294967296;
    };
}

type Seed = number | string | undefined;
type WeightedData<T> = Array<[number, T]>;

class Random {
    static global = new Random();

    seed: Seed;

    generator: () => number;

    constructor(seed?: Seed) {
        if (seed === undefined) {
            seed = Math.random();
        }

        this.seed = seed;
        this.generator = mulberry32(hashString(`${seed}`));
    }

    /**
     * Picks a random item from an array.
     *
     * @param {Array} array Array to pick an item from
     */
    arrayItem<T>(array: Array<T>): T {
        return array[Math.floor(this.float() * array.length)];
    }

    /**
     * Generates either true or false.
     */
    boolean() {
        return this.float() > 0.5;
    }

    /**
     *
     * @param {Array<[number, any]>} data The array of weight, value pairs.
     */
    weighted<T>(data: WeightedData<T>): T {
        const weights = data.map((d) => d[0]);

        const sum = weights.reduce((s, w) => s + w, 0);

        if (isNaN(sum)) {
            throw new Error("There is an invalid weight. Weights sum was NaN");
        }

        const selected = this.float() * sum;
        let acc = 0;

        for (let i = 0; i < weights.length; ++i) {
            const weight = weights[i];

            acc += weight;
            if (selected <= acc) {
                return data[i][1];
            }
        }

        return data[weights.length - 1][1];
    }

    /**
     * Generates a random integer number between the given bounds.
     * If min is specified but max is omitted, max = min and min = 0.
     * If neither is specified, min = 0 and max = MAX_INT.
     * min and max can be returned.
     *
     * @param {integer} min Inclusive
     * @param {integer} max Inclusive
     */
    int(min?: number, max?: number): number {
        if (max === undefined) {
            max = min;
            min = 0;
        }
        if (min === undefined) {
            min = 0;
            max = 214746483647;
        }

        min = Math.ceil(min);
        max = Math.floor(max!);

        return Math.floor(this.float() * (max - min + 1)) + min;
    }

    /**
     * Generates a number between 0 and 1.
     */
    float() {
        return this.generator();
    }

    /**
     * Returns a new seeded Random generator depending on the current state.
     */
    randomGenerator() {
        return new Random(this.int());
    }
}

export default Random;
