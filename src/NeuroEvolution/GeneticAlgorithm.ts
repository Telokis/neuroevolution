import _ from "lodash";
import Random from "./NeuralNetwork/Random";

type Population<T> = Array<T>;
type AgentFactory<T> = () => T;
type FitnessFunction<T> = (agent: T) => number;

export interface IGeneticAlgorithmOptions<T> {
    populationSize: number;
    minFitness?: number;
    duplicateBest?: number;
    skipFirstNMutations?: number;
    factory: AgentFactory<T>;
    fitnessFn: FitnessFunction<T>;
    crossover: (parentA: T, parentB: T, ratio: number) => T;
    mutate: (source: T) => void;
    resetAgent: (agent: T) => T;
    clone: (agent: T) => T;
}

function remap(x: number, inMin: number, inMax: number, outMin: number, outMax: number): number {
    return ((x - inMin) * (outMax - outMin)) / (inMax - inMin) + outMin;
}

export default class GeneticAlgorithm<Agent> {
    population: Population<Agent> = [];

    opts: IGeneticAlgorithmOptions<Agent>;

    generation = 1;

    bestEver: {
        agent: Agent;
        fitness: number;
        generation: number;
    };

    constructor(options: IGeneticAlgorithmOptions<Agent>) {
        this.opts = options;

        if (this.opts.populationSize % 2 !== 0) {
            throw new Error("Population size must be even");
        }

        this.randomizePopulation();

        const bestEverAgent = this.randomAgent();
        this.bestEver = {
            agent: this.opts.clone(bestEverAgent),
            fitness: this.opts.fitnessFn(bestEverAgent),
            generation: this.generation,
        };
    }

    randomizePopulation() {
        this.population = [];
        this.generation = 1;

        for (let i = 0; i < this.opts.populationSize; ++i) {
            const agent = this.opts.factory();

            this.population.push(agent);
        }
    }

    advancePopulation() {
        const {
            fitnessFn,
            clone,
            resetAgent,
            populationSize,
            minFitness,
            crossover,
            mutate,
            duplicateBest,
            skipFirstNMutations,
        } = this.opts;
        let best = this.sliceBestHalf();

        best.splice(1, 0, ..._.times(duplicateBest ?? 0, () => clone(best[0])));
        best = best.slice(0, populationSize);

        const weights = best.map((agent, index) => [best.length - index, agent] as [number, Agent]);

        const bestFitnessInBatch = fitnessFn(best[0]);
        if (bestFitnessInBatch > this.bestEver.fitness) {
            this.bestEver = {
                agent: clone(best[0]),
                fitness: bestFitnessInBatch,
                generation: this.generation,
            };
        }

        this.population = [...best.map((a) => resetAgent(a))];

        const toAdd = populationSize - this.population.length;
        for (let i = 0; i < toAdd; i++) {
            const parentA = Random.global.weighted(weights);
            const parentB = Random.global.weighted(weights);

            const fitA = fitnessFn(parentA);
            const fitB = fitnessFn(parentB);

            const ratio = remap(fitA, minFitness ?? 0, fitA + fitB, 0, 1);
            const newAgent = crossover(parentA, parentB, ratio);

            this.population.push(newAgent);
        }

        for (let i = skipFirstNMutations ?? 0; i < this.population.length; ++i) {
            mutate(this.population[i]);
        }

        this.generation++;
    }

    sortPopulation() {
        const { fitnessFn } = this.opts;

        this.population.sort((a, b) => fitnessFn(b) - fitnessFn(a));
    }

    sliceBestHalf() {
        this.sortPopulation();

        return this.population.slice(0, this.opts.populationSize / 2);
    }

    randomAgent() {
        return Random.global.arrayItem(this.population);
    }

    bestAgent() {
        return _.maxBy(this.population, this.opts.fitnessFn)!;
    }
}
