import EE from "eventemitter3";

import { ValueOf, PickVoidMembers, PickNonVoidMembers } from "@telokys/ts-meta-types";

export type EventEmitterEventsVoid<Events> = PickVoidMembers<Events>;
export type EventEmitterEventsNonVoid<Events> = PickNonVoidMembers<Events>;

class EventEmitter<Events extends {}> {
    private emitter = new EE<string>();

    removeAllCallbacks() {
        this.emitter.removeAllListeners();
    }

    on<K extends keyof EventEmitterEventsNonVoid<Events>>(
        eventName: K,
        callback: (payload: EventEmitterEventsNonVoid<Events>[K]) => void,
    ): void;

    on<K extends keyof EventEmitterEventsVoid<Events>>(eventName: K, callback: () => void): void;

    on(eventName: string, fn: (payload?: ValueOf<Events>) => void) {
        this.emitter.on(eventName, fn);
    }

    off<K extends keyof EventEmitterEventsNonVoid<Events>>(
        eventName: K,
        callback: (payload: EventEmitterEventsNonVoid<Events>[K]) => void,
    ): void;

    off<K extends keyof EventEmitterEventsVoid<Events>>(eventName: K, callback: () => void): void;

    off(eventName: string, fn: (payload?: ValueOf<Events>) => void) {
        this.emitter.off(eventName, fn);
    }

    emit<K extends keyof EventEmitterEventsNonVoid<Events>>(
        eventName: K,
        payload: EventEmitterEventsNonVoid<Events>[K],
    ): void;

    emit<K extends keyof EventEmitterEventsVoid<Events>>(eventName: K): void;

    emit(callbackName: string, payload?: ValueOf<Events>) {
        this.emitter.emit(callbackName, payload);
    }
}

export default EventEmitter;
