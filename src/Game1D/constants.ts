export const OBSTACLE_WIDTH = 20;
export const MAX_FPS = 60;
export const PX_PER_SEC = 400;
export const SPEED = PX_PER_SEC / 1000;
export const PLAYER_SIZE = 50;
export const DESTROY_RANGE = 20;
export const TEXT_SIZE = 24;
export const TEXT_SPACE = 24;

export enum PossibleAction {
    LEFT,
    RIGHT,
    DESTROY,
}

export const POPULATION_SIZE = 500;
export const ACTION_THRESHOLD = 0.5;
export const LIFE_DURATION_TICKS = 100;
export const PX_PER_TICK = 10;

export const MUTATION_RATE = 0.7;
export const NEW_WEIGHT_MUTATION_RATE = 0.2;
export const ALTER_WEIGHT_MUTATION_RATE = 1 - NEW_WEIGHT_MUTATION_RATE;

export const TOPOLOGY: [number, Array<number>, number] = [1, [], 3];
