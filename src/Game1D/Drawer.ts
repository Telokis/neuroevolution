import p5 from "p5";
import { inBox1d } from "./helpers";
import Engine /* , { Scores } */ from "./Engine";
import { OBSTACLE_WIDTH, PLAYER_SIZE, PossibleAction, TEXT_SPACE } from "./constants";

export default class Drawer {
    floorHeight: number;

    playerXOffset: number;

    cameraPos!: number;

    engine: Engine;

    p: p5;

    keyToAction: [number, PossibleAction][] = [];

    txt: { [key: string]: number } = {};

    scaleX: number;

    scaleY: number;

    allowInput: boolean;

    constructor(engine: Engine, p: p5, allowInput = false) {
        this.floorHeight = Math.floor((p.height / 3) * 2);
        this.playerXOffset = Math.floor(p.width / 8);
        this.engine = engine;
        this.p = p;
        this.allowInput = allowInput;

        this.scaleX = p.width / 1000;
        this.scaleY = p.height / 1000;

        this.updateCameraPos();

        this.keyToAction = (Object.entries({
            [p.LEFT_ARROW]: PossibleAction.LEFT,
            [p.RIGHT_ARROW]: PossibleAction.RIGHT,
            [p.UP_ARROW]: PossibleAction.DESTROY,
        }) as unknown) as [number, PossibleAction][];

        for (let i = 0; i < 20; ++i) {
            this.txt[`l${i}`] = (i + 1) * this.scaleY * TEXT_SPACE + this.floorHeight;
        }
    }

    setEngine(engine: Engine, allowInput = false) {
        this.engine = engine;
        this.allowInput = allowInput;
        this.updateCameraPos();
    }

    updateCameraPos() {
        this.cameraPos = this.scaleX * this.engine.playerX - this.playerXOffset;
    }

    keyPressed() {
        if (this.allowInput && this.p.keyCode === this.p.UP_ARROW) {
            this.engine.performAction(PossibleAction.DESTROY, this.p.deltaTime);
        }
    }

    update() {
        if (this.allowInput) {
            if (this.p.keyIsDown(this.p.LEFT_ARROW)) {
                this.engine.performAction(PossibleAction.LEFT, this.p.deltaTime);
            }

            if (this.p.keyIsDown(this.p.RIGHT_ARROW)) {
                this.engine.performAction(PossibleAction.RIGHT, this.p.deltaTime);
            }
        }

        this.updateCameraPos();
    }

    drawPlayer() {
        this.p.stroke(0, 44, 69);
        this.p.strokeWeight(2);
        this.p.fill(0, 104, 139);
        this.p.rect(
            this.playerXOffset,
            this.floorHeight - this.scaleY * PLAYER_SIZE,
            this.scaleX * PLAYER_SIZE,
            this.scaleY * PLAYER_SIZE,
        );
    }

    drawFloor() {
        if (this.engine.score < 0) {
            this.p.noStroke();
            this.p.strokeWeight(2);
            this.p.fill(0xbc, 0x8f, 0x8f);
            this.p.rect(0, 0, this.p.width, this.floorHeight);
        }

        this.p.stroke("black");
        this.p.strokeWeight(2);
        this.p.fill(150);
        this.p.rect(-10, this.floorHeight, this.p.width + 20, this.floorHeight);
    }

    drawObstacles() {
        const w = Math.max(Math.ceil(this.scaleX * OBSTACLE_WIDTH), 1);

        for (const obstacle of this.engine.obstacles) {
            const x = this.scaleX * obstacle.x;

            if (inBox1d(x, w, this.cameraPos, this.p.width)) {
                this.p.stroke("black");
                this.p.strokeWeight(1);
                this.p.fill(150, 34, 34);
                this.p.rect(x - this.cameraPos, 0, w, this.floorHeight);
            }
        }
    }

    drawDebug() {
        // const { score } = this.engine;
        // let deathReason = "Fine";

        // if (score < 0) {
        //     deathReason = Scores[score];
        // }

        this.p.textSize(this.scaleY * 24);
        this.p.noStroke();
        this.p.fill("black");
        this.p.text(
            `Distance to next obstacle: ${Math.floor(this.engine.distanceToNextObstacle())}`,
            10,
            this.txt.l0,
        );
        // this.p.text(`Score: ${Math.floor(this.engine.score)} (${deathReason})`, 10, this.txt.l1);
    }

    draw() {
        this.drawFloor();
        this.drawObstacles();
        this.drawPlayer();
        this.drawDebug();
    }
}
