import p5 from "p5";
import {
    MAX_FPS,
    PossibleAction,
    ACTION_THRESHOLD,
    LIFE_DURATION_TICKS,
    PX_PER_TICK,
    POPULATION_SIZE,
    MUTATION_RATE,
    NEW_WEIGHT_MUTATION_RATE,
    TOPOLOGY,
    SPEED,
} from "./constants";
import Draw from "./Drawer";
import GeneticAlgorithm from "../NeuroEvolution/GeneticAlgorithm";
import Engine from "./Engine";
import NeuralNetwork from "../NeuroEvolution/NeuralNetwork/NeuralNetwork";
import { drawNN } from "../NeuroEvolution/NeuralNetwork/viewNN";
import { formatNumber, toPrecision, IHistoryData, nthPercentile } from "./helpers";
import EventEmitter from "../Helpers/EventEmitter";
import Game1DEvents from "./Events";
import { Speed } from "../components/SpeedSlider";

type Agent = [Engine, NeuralNetwork];

const sketch = (w = 1000, h = 1000, ee: EventEmitter<Game1DEvents>) => (p: p5) => {
    let geneticAlgorithm: GeneticAlgorithm<Agent>;
    let drawer: Draw;
    let currentNN: NeuralNetwork;
    let endless = false;
    let totalTicks = 0;
    let maxTicks = LIFE_DURATION_TICKS;
    let speedInfos: Speed = {
        fps: MAX_FPS,
        ticks: 1,
    };
    const generationHistory: Array<IHistoryData> = [];

    function getInputs(engine: Engine) {
        return [
            /* engine.playerX / 1000,  */
            engine.distanceToNextObstacle() / 100,
        ];
    }

    function simulate() {
        let allDead = true;

        for (const [engine, nn] of geneticAlgorithm.population) {
            if (engine.score >= 0) {
                const inputs = getInputs(engine);
                const results = nn.predict(inputs);
                let { score } = engine;

                if (score >= 0) {
                    engine.tick();
                }

                if (score >= 0 && results[2] > ACTION_THRESHOLD) {
                    score = engine.performAction(PossibleAction.DESTROY, PX_PER_TICK);
                }

                if (score >= 0 && results[0] > ACTION_THRESHOLD) {
                    score = engine.performAction(PossibleAction.LEFT, PX_PER_TICK);
                }

                if (score >= 0 && results[1] > ACTION_THRESHOLD) {
                    score = engine.performAction(PossibleAction.RIGHT, PX_PER_TICK);
                }

                if (score >= 0) {
                    allDead = false;
                }
            }
        }

        return allDead;
    }

    function mathFitness(score: number) {
        return Math.floor(score);
    }

    function fitness(engine: { maxScore: number; score: number }) {
        return mathFitness(engine.score);
        // return mathFitness(engine.maxScore ** 2 - Math.min(0, engine.score) ** 2);
    }

    let maxFitnessPossible: number;

    function refreshMaxPossibleFitness() {
        const mScore = SPEED * PX_PER_TICK * maxTicks;

        maxFitnessPossible = fitness({ maxScore: mScore, score: mScore });
        ee.emit("setMaxPossibleFitness", maxFitnessPossible);
    }

    function archivePopulation() {
        const agents = geneticAlgorithm.population.map(([engine]) => ({
            score: engine.score,
            maxScore: engine.maxScore,
            fitness: fitness(engine),
            status: engine.status,
            idleRate: toPrecision((engine.idleTicks / engine.totalTicks) * 100),
            actionsPerTick: toPrecision(engine.performedActions / engine.totalTicks),
        }));

        const archive: IHistoryData = {
            statuses: {},
            generation: geneticAlgorithm.generation,
            fitness: {
                average: 0,
                median: 0,
                p99: 0,
                p90: 0,
                p75: 0,
                p25: 0,
                p10: 0,
            },
            averageIdleRate: 0,
            averageActionsPerTick: 0,
        };

        agents.sort((a, b) => b.fitness - a.fitness);

        for (const agent of agents) {
            archive.fitness.average += agent.fitness;
            archive.averageIdleRate += agent.idleRate;
            archive.averageActionsPerTick += agent.actionsPerTick;

            if (!(agent.status in archive.statuses)) {
                archive.statuses[agent.status] = 0;
            }

            archive.statuses[agent.status] += 1;
        }

        archive.fitness.average = toPrecision(archive.fitness.average / agents.length, 4);
        archive.averageIdleRate = toPrecision(archive.averageIdleRate / agents.length, 4);
        archive.averageActionsPerTick = toPrecision(
            archive.averageActionsPerTick / agents.length,
            4,
        );

        archive.fitness.median = nthPercentile(agents, 0.5, "fitness", true);
        archive.fitness.p99 = nthPercentile(agents, 0.99, "fitness", true);
        archive.fitness.p90 = nthPercentile(agents, 0.9, "fitness", true);
        archive.fitness.p75 = nthPercentile(agents, 0.75, "fitness", true);
        archive.fitness.p25 = nthPercentile(agents, 0.25, "fitness", true);
        archive.fitness.p10 = nthPercentile(agents, 0.1, "fitness", true);

        generationHistory.push(archive);
        ee.emit("newHistoryData", archive);
    }

    function renewPopulation() {
        archivePopulation();
        geneticAlgorithm.advancePopulation();
    }

    function mutate(agent: Agent) {
        agent[0] = new Engine();

        const perform = (val: number) => {
            if (Math.random() < MUTATION_RATE) {
                if (Math.random() < NEW_WEIGHT_MUTATION_RATE) {
                    return p.randomGaussian(0, 10);
                }

                return val + p.randomGaussian(0, 1);
            }

            return val;
        };

        for (const layer of agent[1].layers) {
            layer.biases.fill((_1, _2, val) => perform(val));
            layer.weights.fill((_1, _2, val) => perform(val));
        }
    }

    function crossover(parentA: Agent, parentB: Agent, ratio: number): Agent {
        const child = NeuralNetwork.fromJSON(parentA[1].toJSON());

        for (let i = 0; i < child.layers.length; i++) {
            const layer = child.layers[i];
            const layerA = parentA[1].layers[i];
            const layerB = parentB[1].layers[i];

            layer.biases.fill((row, col) =>
                Math.random() < ratio ? layerA.biases.at(row, col) : layerB.biases.at(row, col),
            );
            layer.weights.fill((row, col) =>
                Math.random() < ratio ? layerA.weights.at(row, col) : layerB.weights.at(row, col),
            );
        }

        return [new Engine(), child];
    }

    function update() {
        p.frameRate(speedInfos.fps);

        for (let i = 0; i < speedInfos.ticks; ++i) {
            const allDead = simulate();
            totalTicks += 1;

            if (allDead || (totalTicks >= maxTicks && !endless)) {
                totalTicks = 0;
                renewPopulation();
                drawer.setEngine(geneticAlgorithm.population[0][0]);
                currentNN = geneticAlgorithm.population[0][1];
            }
        }

        geneticAlgorithm.sortPopulation();
        const [currentEngine, nn] = geneticAlgorithm.population[0];
        drawer.setEngine(currentEngine);
        currentNN = nn;
    }

    p.setup = function setup() {
        p.createCanvas(w, h);
        p.frameRate(MAX_FPS);
        p.noSmooth();
        refreshMaxPossibleFitness();

        const exportBestEver = () => {
            console.log("Generation", geneticAlgorithm.bestEver.generation);
            console.log("Fitness", geneticAlgorithm.bestEver.fitness);
            console.log(
                `gen ${geneticAlgorithm.bestEver.generation}, fit ${Math.floor(
                    geneticAlgorithm.bestEver.fitness,
                )}`,
            );
            console.log(geneticAlgorithm.bestEver.agent[1].toJSON());
            console.log(JSON.stringify(geneticAlgorithm.bestEver.agent[1].toJSON()));
        };

        ee.on("exportBestEver", exportBestEver);
        ee.on("setEndlessMode", (val) => {
            endless = val;
        });
        ee.on("setMaxTicks", (val) => {
            maxTicks = val;
            refreshMaxPossibleFitness();
        });
        ee.on("updateSpeed", (val) => {
            speedInfos = val;
        });

        geneticAlgorithm = new GeneticAlgorithm({
            populationSize: POPULATION_SIZE,
            skipFirstNMutations: 1,
            duplicateBest: Math.ceil(POPULATION_SIZE * 0.05),
            factory: () => [new Engine(), new NeuralNetwork(...TOPOLOGY)],
            fitnessFn: ([engine]) => fitness(engine),
            mutate,
            crossover,
            resetAgent: (agent) => [new Engine(), agent[1]],
            clone: (agent) => [new Engine(), NeuralNetwork.fromJSON(agent[1].toJSON())],
        });

        const [currentEngine] = geneticAlgorithm.randomAgent();

        drawer = new Draw(currentEngine, p, false);
    };

    p.keyPressed = function keyPressed() {
        drawer.keyPressed();
    };

    p.draw = function draw() {
        p.background(40);

        update();
        drawer.draw();

        p.text(`Current generation: ${geneticAlgorithm.generation}`, 10, drawer.txt.l2);
        p.text(`Current fitness: ${formatNumber(fitness(drawer.engine))}`, 10, drawer.txt.l3);
        p.text(`Tick: ${totalTicks}/${endless ? "Endless" : maxTicks}`, 10, drawer.txt.l4);
        p.text(
            `Best ever: Gen ${geneticAlgorithm.bestEver.generation}, Fit: ${formatNumber(
                geneticAlgorithm.bestEver.fitness,
            )}`,
            10,
            drawer.txt.l6,
        );
        p.text(`Max possible fitness: ${formatNumber(maxFitnessPossible)}`, 10, drawer.txt.l7);

        const inputs = getInputs(drawer.engine);
        drawNN(currentNN, p, inputs);
    };
};

export default sketch;
