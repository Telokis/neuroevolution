import React, { FC, useRef, useEffect, useMemo, useState } from "react";
import p5 from "p5";
import {
    Grid,
    Button,
    ButtonGroup,
    makeStyles,
    FormControlLabel,
    Switch,
    Slider,
    Typography,
} from "@material-ui/core";
import sketch from "./sketch";
import Game1DEvents from "./Events";
import EventEmitter, {
    EventEmitterEventsVoid,
    EventEmitterEventsNonVoid,
} from "../Helpers/EventEmitter";
import SpeedSlider from "../components/SpeedSlider";
import { MAX_FPS, LIFE_DURATION_TICKS } from "./constants";
import OpaqueTooltip from "../components/OpaqueTooltip";
import GameStats from "./GameStats";
import { IHistoryData } from "./helpers";

const DEFAULT_SPEED = { fps: MAX_FPS, ticks: 1 };

const useStyles = makeStyles({
    canvasDiv: {
        width: "1020px",
        maxWidth: "1020px",
        flexBasis: "1020px",
    },
    toolsDiv: {
        width: "350px",
        maxWidth: "350px",
        flexBasis: "350px",
    },
});

function useSmartEmit<K extends keyof EventEmitterEventsNonVoid<Game1DEvents>>(
    ee: EventEmitter<Game1DEvents>,
    eventName: K,
    payload: EventEmitterEventsNonVoid<Game1DEvents>[K],
): void;
function useSmartEmit<K extends keyof EventEmitterEventsVoid<Game1DEvents>>(
    ee: EventEmitter<Game1DEvents>,
    eventName: K,
): void;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function useSmartEmit(ee: EventEmitter<Game1DEvents>, event: any, payload?: any) {
    const depStr = JSON.stringify(payload);

    useEffect(() => {
        ee.emit(event, payload);
    }, [depStr, ee, event]); // eslint-disable-line react-hooks/exhaustive-deps
}

const PageGame1D: FC = () => {
    const styles = useStyles();
    const ref = useRef<HTMLDivElement>(null);
    const ee = useMemo(() => new EventEmitter<Game1DEvents>(), []);
    const [speed, setSpeed] = useState(DEFAULT_SPEED);
    const [endless, setEndless] = useState(false);
    const [maxTicks, setMaxTicks] = useState(LIFE_DURATION_TICKS);
    const [historyData, setHistoryData] = useState<Array<IHistoryData>>([]);
    const [maxPossibleFitness, setMaxPossibleFitness] = useState(0);

    useSmartEmit(ee, "updateSpeed", speed);
    useSmartEmit(ee, "setEndlessMode", endless);
    useSmartEmit(ee, "setMaxTicks", maxTicks);

    useEffect(() => {
        ee.on("newHistoryData", (historyDataEntry) => {
            setHistoryData((prevHistoryData) => [...prevHistoryData, historyDataEntry].slice(-500));
        });
    }, [ee]);

    useEffect(() => {
        ee.on("setMaxPossibleFitness", (fit) => {
            setMaxPossibleFitness(fit);
        });
    }, [ee]);

    useEffect(() => {
        new p5(sketch(1000, 900, ee), ref.current!);
    }, [ee]);

    return (
        <Grid container direction="row" justify="center" alignItems="center" spacing={3}>
            <GameStats history={historyData} maxPossibleFitness={maxPossibleFitness} />
            <Grid item xs={6} className={styles.canvasDiv}>
                <div ref={ref} />
            </Grid>
            <Grid
                container
                item
                justify="center"
                alignItems="center"
                spacing={3}
                xs={3}
                className={styles.toolsDiv}
            >
                <Grid item container justify="center" alignItems="center" spacing={1} xs={12}>
                    <Grid item style={{ textAlign: "center" }} xs={12}>
                        <ButtonGroup variant="contained" orientation="vertical" color="primary">
                            <OpaqueTooltip
                                placement="top"
                                title={
                                    <Typography display="block">
                                        Will print informations about the best ever in the
                                        browser&apos;s console
                                    </Typography>
                                }
                            >
                                <Button
                                    variant="contained"
                                    onClick={() => ee.emit("exportBestEver")}
                                >
                                    Export best ever
                                </Button>
                            </OpaqueTooltip>
                        </ButtonGroup>
                    </Grid>
                    <Grid item style={{ textAlign: "center" }} xs={12}>
                        <OpaqueTooltip
                            placement="top"
                            title={
                                <Typography display="block">
                                    How many ticks the AIs will play the game for
                                </Typography>
                            }
                        >
                            <div
                                style={{
                                    marginRight: "10px",
                                    marginLeft: "10px",
                                    textAlign: "left",
                                }}
                            >
                                <Typography variant="subtitle1" display="inline">
                                    Max ticks per simulations
                                </Typography>
                                <Slider
                                    defaultValue={LIFE_DURATION_TICKS}
                                    value={maxTicks}
                                    step={10}
                                    track={false}
                                    valueLabelDisplay="auto"
                                    min={10}
                                    max={2000}
                                    onChange={(_, newVal: number) => {
                                        setMaxTicks(newVal);
                                    }}
                                />
                            </div>
                        </OpaqueTooltip>
                    </Grid>
                    <OpaqueTooltip
                        placement="top"
                        title={
                            <Typography display="block">
                                Disables the tick limit: the AIs can live indefinitely
                            </Typography>
                        }
                    >
                        <Grid item style={{ textAlign: "center" }} xs={12}>
                            <FormControlLabel
                                control={
                                    <Switch
                                        checked={endless}
                                        onChange={(event) => {
                                            setEndless(event.target.checked);
                                        }}
                                    />
                                }
                                label="Endless mode"
                            />
                        </Grid>
                    </OpaqueTooltip>
                </Grid>
                <Grid item xs={12}>
                    <SpeedSlider
                        value={speed}
                        maxFPS={MAX_FPS}
                        max={300}
                        onUpdate={(newSpeed) => {
                            setSpeed(newSpeed);
                        }}
                    />
                </Grid>
            </Grid>
        </Grid>
    );
};

export default PageGame1D;
