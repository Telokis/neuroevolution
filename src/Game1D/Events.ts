import { Speed } from "../components/SpeedSlider";
import { IHistoryData } from "./helpers";

export default interface Game1DEvents {
    exportBestEver: void;
    updateSpeed: Speed;
    setEndlessMode: boolean;
    setMaxTicks: number;
    newHistoryData: IHistoryData;
    setMaxPossibleFitness: number;
}
