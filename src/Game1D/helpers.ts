import { ObjectOf } from "@telokys/ts-meta-types";

export interface IHistoryData {
    statuses: ObjectOf<number>;
    generation: number;
    averageIdleRate: number;
    averageActionsPerTick: number;
    fitness: {
        average: number;
        median: number;
        p99: number;
        p90: number;
        p75: number;
        p25: number;
        p10: number;
    };
}

export function inBox1d(x1: number, w1: number, x2: number, w2: number) {
    if (x2 + w2 < x1) return false;
    if (x2 > x1 + w1) return false;

    return true;
}

export function inBox2d(
    x1: number,
    y1: number,
    w1: number,
    h1: number,
    x2: number,
    y2: number,
    w2: number,
    h2: number,
) {
    if (y2 + h2 < y1) return false;
    if (y2 > y1 + h1) return false;

    return inBox1d(x1, w1, x2, w2);
}

export function findNextInArr<T extends {} | number, K extends keyof T>(
    num: number,
    arr: Array<T>,
    prop?: K,
): T | null {
    for (const val of arr) {
        if (typeof val === "number") {
            if (val > num) {
                return val;
            }
        } else if (prop) {
            const subVal = val[prop];

            if (typeof subVal === "number" && subVal > num) {
                return val;
            }
        }
    }

    return null;
}

export function binarySearch<T>(
    arr: Array<T>,
    element: T,
    comp: (a: T, b: T) => number = (a, b) =>
        ((a as unknown) as number) - ((b as unknown) as number),
) {
    if (arr.length < 1) {
        return 0;
    }

    if (comp(element, arr[0]) < 0) {
        return 0;
    }

    if (comp(element, arr[arr.length - 1]) > 0) {
        return arr.length;
    }

    let begin = 0;
    let end = arr.length - 1;

    while (begin <= end) {
        const k = (end + begin) >> 1;
        const cmp = comp(element, arr[k]);

        if (cmp > 0) {
            begin = k + 1;
        } else if (cmp < 0) {
            end = k - 1;
        } else {
            return k;
        }
    }

    return -begin - 1;
}

export function binarySearchBy<T extends {}, K extends keyof T = keyof T>(
    arr: Array<T>,
    element: T,
    prop: K,
) {
    return binarySearch(
        arr,
        element,
        (a, b) => ((a[prop] as unknown) as number) - ((b[prop] as unknown) as number),
    );
}

export function formatNumber(num: number) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");
}

export function toPrecision(num: number, precision = 2) {
    const e = 10 ** precision;

    return Math.floor(num * e) / e;
}

export function nthPercentile<
    T,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    K extends keyof any &
        {
            [K in keyof T]: T[K] extends number ? K : never;
        }[keyof T]
>(arr: Array<T>, p: number, key: K, reverseOrder = false): T[K] {
    const index = arr.length * (reverseOrder ? 1 - p : p);

    if (Math.floor(index) !== index) {
        const res = arr[Math.floor(index)];

        return res[key];
    }

    return arr[index][key];
}
