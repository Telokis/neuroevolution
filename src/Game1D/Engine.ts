import { findNextInArr, inBox1d, binarySearchBy } from "./helpers";
import { SPEED, PLAYER_SIZE, DESTROY_RANGE, PossibleAction, OBSTACLE_WIDTH } from "./constants";
import Random from "../NeuroEvolution/NeuralNetwork/Random";

interface Obstacle {
    x: number;
}

export enum DeathReason {
    COLLISION = "COLLISION",
    NOTHING_IN_RANGE = "NOTHING_IN_RANGE",
    OUT_OF_BOUNDS = "OUT_OF_BOUNDS",
}

export default class Engine {
    obstacles = [742, 998, 130, 58, 270, 1500, 8495648].map((x) => ({ x }));

    playerX = 0;

    death: null | DeathReason = null;

    score = 0;

    maxScore = 0;

    performedActions = 0;

    idleTicks = 0;

    totalTicks = 0;

    actionsSinceLastTick = 0;

    constructor() {
        this.obstacles.sort((a, b) => a.x - b.x);
    }

    get isDead() {
        return this.death !== null;
    }

    generateRandomObstacle() {
        const obstacle = {
            x: Random.global.int(
                this.playerX + DESTROY_RANGE + PLAYER_SIZE + 1,
                this.playerX + 1000,
            ),
        };
        const index = binarySearchBy(this.obstacles, obstacle, "x");

        this.obstacles.splice(index, 0, obstacle);
    }

    computeScore() {
        if (this.isDead) {
            this.score = 0;

            return this.score;
        }

        for (const obstacle of this.obstacles) {
            if (inBox1d(obstacle.x, OBSTACLE_WIDTH, this.playerX, PLAYER_SIZE)) {
                this.death = DeathReason.COLLISION;
                this.score = 0;

                return this.score;
            }
        }

        if (this.playerX < 0) {
            this.death = DeathReason.OUT_OF_BOUNDS;
            this.score = 0;

            return this.score;
        }

        this.maxScore = Math.max(this.maxScore, this.score);

        return this.score;
    }

    getNextObstacle(pos?: number) {
        const playerPos = pos ?? this.playerX + PLAYER_SIZE;

        return findNextInArr(playerPos, this.obstacles, "x");
    }

    distanceToNextObstacle<T extends Obstacle>(obstacle?: T) {
        const playerPos = this.playerX + PLAYER_SIZE;
        const nextObstacle = obstacle ?? this.getNextObstacle(playerPos);

        if (!nextObstacle) {
            return -1;
        }

        return nextObstacle.x - playerPos;
    }

    tryToDestroy() {
        const nextObstacle = this.getNextObstacle();

        if (!nextObstacle) {
            this.death = DeathReason.NOTHING_IN_RANGE;
            this.score = 0;

            return;
        }

        const distance = this.distanceToNextObstacle(nextObstacle);

        if (distance <= DESTROY_RANGE) {
            this.obstacles.splice(0, 1);
            this.generateRandomObstacle();
        } else {
            this.death = DeathReason.NOTHING_IN_RANGE;
            this.score = 0;
        }
    }

    get status() {
        if (this.isDead) {
            return this.death!;
        }

        return "Fine";
    }

    tick() {
        if (this.actionsSinceLastTick === 0) {
            this.idleTicks += 1;
        }

        this.totalTicks += 1;
        this.actionsSinceLastTick = 0;
    }

    performAction(action: PossibleAction, delta: number) {
        this.performedActions += 1;
        this.actionsSinceLastTick += 1;

        if (!this.isDead) {
            if (action === PossibleAction.LEFT) {
                this.playerX -= SPEED * delta;
                this.score -= SPEED * delta;
            }

            if (action === PossibleAction.RIGHT) {
                this.playerX += SPEED * delta;
                this.score += SPEED * delta;
            }

            if (action === PossibleAction.DESTROY) {
                this.tryToDestroy();
            }
        }

        return this.computeScore();
    }
}
