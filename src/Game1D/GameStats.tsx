import React, { FC, useMemo } from "react";
import { Grid, makeStyles, Theme } from "@material-ui/core";
import {
    LineChart,
    AreaChart,
    Area,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ReferenceLine,
} from "recharts";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { IHistoryData } from "./helpers";
import { POPULATION_SIZE } from "./constants";

interface GameStatsProps {
    history: Array<IHistoryData>;
    maxPossibleFitness: number;
}

const screen = {
    lg: {
        cellWidth: 533,
        cellHeight: 290,
        elems: 300,
    },
    xl: {
        cellWidth: 1173,
        cellHeight: 410,
        elems: 300,
    },
};

const useStyles = makeStyles((theme: Theme) => ({
    statsPanel: {
        width: `${screen.lg.cellWidth}px`,
        maxWidth: `${screen.lg.cellWidth}px`,
        flexBasis: `${screen.lg.cellWidth}px`,

        [theme.breakpoints.up(1921)]: {
            width: `${screen.xl.cellWidth}px`,
            maxWidth: `${screen.xl.cellWidth}px`,
            flexBasis: `${screen.xl.cellWidth}px`,
        },
    },
    statsCell: {
        padding: 0,
    },
}));

const GameStats: FC<GameStatsProps> = ({ history, maxPossibleFitness }: GameStatsProps) => {
    const styles = useStyles();
    const matches = useMediaQuery((theme: Theme) => theme.breakpoints.up(1921));
    const { cellWidth, cellHeight, elems } = screen[matches ? "xl" : "lg"];

    const deathReasonData = useMemo(() => {
        return history.slice(-1 * elems).map((h) => {
            const res = {
                name: `Gen ${h.generation}`,
                COLLISION: h.statuses.COLLISION ?? 0,
                NOTHING_IN_RANGE: h.statuses.NOTHING_IN_RANGE ?? 0,
                OUT_OF_BOUNDS: h.statuses.OUT_OF_BOUNDS ?? 0,
                Fine: h.statuses.Fine ?? 0,
            };

            return res;
        });
    }, [history, elems]);

    const fitnessData = useMemo(() => {
        return history.slice(-1 * elems).map((h) => {
            const res = {
                name: `Gen ${h.generation}`,
                average: h.fitness.average,
                median: h.fitness.median,
                p99: h.fitness.p99,
                p90: h.fitness.p90,
                p75: h.fitness.p75,
                p25: h.fitness.p25,
                p10: h.fitness.p10,
            };

            return res;
        });
    }, [history, elems]);

    const actionsData = useMemo(() => {
        return history.slice(-1 * elems).map((h) => {
            const res = {
                name: `Gen ${h.generation}`,
                averageIdleRate: h.averageIdleRate,
                averageActionsPerTick: h.averageActionsPerTick,
            };

            return res;
        });
    }, [history, elems]);

    return (
        <Grid
            container
            item
            justify="center"
            alignItems="center"
            spacing={0}
            xs={3}
            className={styles.statsPanel}
        >
            <Grid item className={styles.statsCell}>
                <AreaChart
                    width={cellWidth}
                    height={cellHeight}
                    data={deathReasonData}
                    margin={{
                        top: 5,
                        right: 30,
                        left: 20,
                        bottom: 5,
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis domain={[0, POPULATION_SIZE]} />
                    <Tooltip contentStyle={{ backgroundColor: "#333" }} />
                    <Legend />
                    <Area
                        type="monotone"
                        dataKey="Fine"
                        stackId="1"
                        stroke="#8884d8"
                        fill="#8884d8"
                    />
                    <Area
                        type="monotone"
                        dataKey="OUT_OF_BOUNDS"
                        stackId="1"
                        stroke="#82ca9d"
                        fill="#82ca9d"
                    />
                    <Area
                        type="monotone"
                        dataKey="NOTHING_IN_RANGE"
                        stackId="1"
                        stroke="#ffc658"
                        fill="#ffc658"
                    />
                    <Area
                        type="monotone"
                        dataKey="COLLISION"
                        stackId="1"
                        stroke="#00688b"
                        fill="#00688b"
                    />
                </AreaChart>
            </Grid>
            <Grid item className={styles.statsCell}>
                <LineChart
                    width={cellWidth}
                    height={cellHeight}
                    data={fitnessData}
                    margin={{
                        top: 5,
                        right: 30,
                        left: 20,
                        bottom: 5,
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip contentStyle={{ backgroundColor: "#333" }} />
                    <Legend />
                    <ReferenceLine y={maxPossibleFitness} label="Max fitness" stroke="white" />
                    <Line type="monotone" dataKey="p99" stroke="#ffc658" />
                    <Line type="monotone" dataKey="p90" stroke="#00688b" />
                    <Line type="monotone" dataKey="p75" stroke="#838B8B" />
                    <Line type="monotone" dataKey="average" stroke="#8884d8" />
                    <Line type="monotone" dataKey="median" stroke="#82ca9d" />
                    <Line type="monotone" dataKey="p25" stroke="#8968CD" />
                    <Line type="monotone" dataKey="p10" stroke="#5DFC0A" />
                </LineChart>
            </Grid>
            <Grid item className={styles.statsCell}>
                <LineChart
                    width={cellWidth}
                    height={cellHeight}
                    data={actionsData}
                    margin={{
                        top: 5,
                        right: 30,
                        left: 20,
                        bottom: 5,
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis yAxisId="left" />
                    <YAxis yAxisId="right" orientation="right" />
                    <Tooltip contentStyle={{ backgroundColor: "#333" }} />
                    <Legend />
                    <Line
                        yAxisId="left"
                        name="Average idle ticks (Left)"
                        type="monotone"
                        dataKey="averageIdleRate"
                        stroke="#ffc658"
                    />
                    <Line
                        yAxisId="right"
                        name="Average action count per tick (Right)"
                        type="monotone"
                        dataKey="averageActionsPerTick"
                        stroke="#00688b"
                    />
                </LineChart>
            </Grid>
        </Grid>
    );
};

export default GameStats;
