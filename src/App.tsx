import React, { FC } from "react";
import { AppBar, Toolbar, Grid, Typography, Container } from "@material-ui/core";
import { ThemeProvider, createMuiTheme, withStyles, createStyles } from "@material-ui/core/styles";
import PageGame1D from "./Game1D/PageGame1D";
import Link from "./components/Link";

const theme = createMuiTheme({
    overrides: {},
    palette: {
        type: "dark",
        primary: {
            main: "#00688B",
        },
        secondary: {
            main: "#7AC5CD",
        },
    },
});

const styles = () =>
    createStyles({
        "@global": {
            body: {
                height: "100%",
                background: "#222",
                margin: 0,
                padding: 0,
                overflowX: "hidden",
                minWidth: "320px",
                fontFamily: "Roboto,'Helvetica Neue',Arial,Helvetica,sans-serif",
                fontSize: "15px",
                lineHeight: "1.4285em",
                color: "white",
                boxSizing: "inherit",
            },
            html: {
                fontSize: "15px",
                boxSizing: "border-box",
                height: "100%",
            },
            a: {
                color: "#ffffff",
                "&:hover": {
                    textShadow: "1px 1px black",
                    color: "#ffffff",
                },
            },
            p: {
                margin: "0 0 1rem",
            },
        },
    });

const App: FC = () => {
    return (
        <ThemeProvider theme={theme}>
            <AppBar position="relative">
                <Toolbar variant="dense">
                    <Grid justify="space-between" container>
                        <Grid item>
                            <Typography>Neuroevolution fun</Typography>
                        </Grid>

                        <Grid item>
                            <Link to="https://gitlab.com/Telokis/neuroevolution">Source code</Link>
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
            <Container maxWidth={false}>
                <PageGame1D />
            </Container>
        </ThemeProvider>
    );
};

export default withStyles(styles)(App);
