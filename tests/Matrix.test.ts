import Matrix from "../src/NeuroEvolution/NeuralNetwork/Matrix";

describe("Matrix", () => {
    it("should do a matrix product", () => {
        const m = new Matrix(2, 3);
        m.data[0] = [1, 2, 3];
        m.data[1] = [4, 5, 6];

        const n = new Matrix(3, 2);
        n.data[0] = [7, 8];
        n.data[1] = [9, 10];
        n.data[2] = [11, 12];

        const mn = Matrix.multiply(m, n);

        expect(mn).toEqual({
            rows: 2,
            columns: 2,
            data: [
                [58, 64],
                [139, 154],
            ],
        });
    });

    it("should throw if matrix multiplication is impossible", () => {
        const m = new Matrix(2, 2);
        const n = new Matrix(3, 3);

        expect(() => {
            Matrix.multiply(m, n);
        }).toThrowError("m1.columns must equal m2.rows");
    });

    it("should generate a Matrix from an array", () => {
        const m = Matrix.fromArray([1, 2, 3]);

        expect(m).toEqual({
            rows: 3,
            columns: 1,
            data: [[1], [2], [3]],
        });
    });

    it("should fill a matrix with a value", () => {
        const m = new Matrix(2, 2);

        m.fill(184);

        expect(m).toEqual({
            rows: 2,
            columns: 2,
            data: [
                [184, 184],
                [184, 184],
            ],
        });
    });

    it("should return the proper value when using at", () => {
        const m = Matrix.fromArray([1, 2, 3]);

        expect(m.at(2, 0)).toEqual(3);
    });

    it("should throw if at is out of bounds for row", () => {
        const m = Matrix.fromArray([1, 2, 3]);

        expect(() => m.at(5, 0)).toThrowError(/row is out of bounds/i);
    });

    it("should throw if at is out of bounds for col", () => {
        const m = Matrix.fromArray([1, 2, 3]);

        expect(() => m.at(0, 5)).toThrowError(/col is out of bounds/i);
    });

    it("should add a scalar to a matrix", () => {
        const m = new Matrix(3, 3);
        m.data[0] = [1, 2, 3];
        m.data[1] = [4, 5, 6];
        m.data[2] = [7, 8, 9];

        m.add(1);

        expect(m).toEqual({
            rows: 3,
            columns: 3,
            data: [
                [2, 3, 4],
                [5, 6, 7],
                [8, 9, 10],
            ],
        });
    });

    it("should add a matrix to another matrix", () => {
        const m = new Matrix(2, 2);
        m.data[0] = [1, 2];
        m.data[1] = [3, 4];

        const n = new Matrix(2, 2);
        n.data[0] = [10, 11];
        n.data[1] = [12, 13];

        m.add(n);

        expect(m).toEqual({
            rows: 2,
            columns: 2,
            data: [
                [11, 13],
                [15, 17],
            ],
        });
    });

    it("should throw an error when adding two different sized matrices", () => {
        const m = new Matrix(2, 2);
        const n = new Matrix(3, 2);

        expect(() => m.add(n)).toThrowError("m1.columns must equal m2.rows");
    });

    it("should be convertible to JSON and back", () => {
        const m = new Matrix(2, 2);
        const json = m.toJSON();
        const n = Matrix.fromJSON(json);

        expect(m).toEqual(n);
    });
});
