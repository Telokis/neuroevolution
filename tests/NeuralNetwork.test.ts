import NeuralNetwork from "../src/NeuroEvolution/NeuralNetwork/NeuralNetwork";

describe("NeuralNetwork", () => {
    it("should create a basic network", () => {
        const nn = new NeuralNetwork(1, [2], 3);

        expect(nn.layers.length).toStrictEqual(2);
        expect(nn.inputDimension).toStrictEqual(1);

        expect(nn.layers[0].fromDimension).toStrictEqual(1);
        expect(nn.layers[0].toDimension).toStrictEqual(2);

        expect(nn.layers[1].fromDimension).toStrictEqual(2);
        expect(nn.layers[1].toDimension).toStrictEqual(3);
    });

    it("should create a network with no hidden layer", () => {
        const nn = new NeuralNetwork(1, [], 3);

        expect(nn.layers.length).toStrictEqual(1);
        expect(nn.inputDimension).toStrictEqual(1);

        expect(nn.layers[0].fromDimension).toStrictEqual(1);
        expect(nn.layers[0].toDimension).toStrictEqual(3);
    });

    it("should create a network with multiple hidden layers", () => {
        const nn = new NeuralNetwork(1, [4, 5, 6], 3);

        expect(nn.layers.length).toStrictEqual(4);
        expect(nn.inputDimension).toStrictEqual(1);

        expect(nn.layers[0].fromDimension).toStrictEqual(1);
        expect(nn.layers[0].toDimension).toStrictEqual(4);

        expect(nn.layers[1].fromDimension).toStrictEqual(4);
        expect(nn.layers[1].toDimension).toStrictEqual(5);

        expect(nn.layers[2].fromDimension).toStrictEqual(5);
        expect(nn.layers[2].toDimension).toStrictEqual(6);

        expect(nn.layers[3].fromDimension).toStrictEqual(6);
        expect(nn.layers[3].toDimension).toStrictEqual(3);
    });

    it("should be convertible to JSON and back", () => {
        const m = new NeuralNetwork(1, [4, 5, 6], 3);
        const json = m.toJSON();

        const n = NeuralNetwork.fromJSON(json);

        expect(m).toEqual(n);
    });
});
