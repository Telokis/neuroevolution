import Layer from "../src/NeuroEvolution/NeuralNetwork/Layer";

describe("Layer", () => {
    it("should handle a basic feed", () => {
        const layer = new Layer(2, 2);

        layer.weights.fill(2);
        layer.biases.fill(5);

        const res = layer.feed([1, 1], (val) => val);

        expect(res).toStrictEqual([9, 9]);
    });

    it("should be convertible to JSON and back", () => {
        const m = new Layer(2, 2);
        const json = m.toJSON();
        const n = Layer.fromJSON(json);

        expect(m).toEqual(n);
    });
});
