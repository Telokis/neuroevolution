const path = require("path");
const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const env = process.env.NODE_ENV;

module.exports = {
    entry: {
        main: "./src/index.tsx",
    },

    mode: env,

    output: {
        path: path.resolve(__dirname, "build"),
        filename: "[name].[hash].bundle.js",
        publicPath: "",
    },

    devServer: {
        contentBase: path.join(__dirname, "public"),
        compress: false,
        port: 3500,
        hot: true,
        open: true,
        clientLogLevel: "silent",
    },

    module: {
        rules: [
            {
                test: /\.(jsx?|tsx?)$/,
                exclude: /node_modules/,
                use: "babel-loader",
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    env === "development" ? "style-loader" : MiniCssExtractPlugin.loader,
                    "css-loader",
                    "postcss-loader",
                ],
            },
        ],
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: "./public/index.html",
            inject: true,
        }),
        new webpack.HotModuleReplacementPlugin(),
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css",
        }),
    ],

    resolve: {
        extensions: [".js", ".jsx", ".ts", ".tsx"],
    },
};
